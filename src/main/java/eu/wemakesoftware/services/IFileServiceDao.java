package eu.wemakesoftware.services;

import eu.wemakesoftware.domain.File;
import eu.wemakesoftware.domain.Permission;
import eu.wemakesoftware.domain.User;

import java.util.List;

/**
 * User: alex
 * Date: 20/04/2016
 * Time: 23:15
 */
public interface IFileServiceDao {



    /**
     * Finds all files for the user.
     *
     * @param user - user to find files for
     * @return list of files
     */
    //TODO implement
    List<File> findFilesRelatedToUser(User user);


    /**
     * Deletes the files from the system.
     *
     *
     * @param file file to delete
     * @param forceDelete if true, the file should be always deleted. if false, the file is deleted only when no user has any permissions to it
     */
    //TODO implement
    void deleteFile(File file, boolean forceDelete);


    /**
     * Adds the file to the system
     *
     * @param file file to add
     * @param user user to link
     * @param permission permission to set
     * @return saved file
     */
    //TODO implement
    File addFile(File file, User user, Permission permission);

    /**
     * Add permission to a file.
     *
     * @param file file to update
     * @param user user to link
     * @param permission permission to add
     *
     * @return saved file
     */
    //TODO implement
    File addPermissionToFile(File file, User user, Permission permission);

}
