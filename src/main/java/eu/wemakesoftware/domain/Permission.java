package eu.wemakesoftware.domain;

import java.util.List;

/**
 * User: alex
 * Date: 20/04/2016
 * Time: 23:12
 */
public class Permission {

    private enum PermissionEnum {
        READ, WRITE, DELETE
    }


    private User user;

    private File file;

    private List<PermissionEnum> permissions;

}
