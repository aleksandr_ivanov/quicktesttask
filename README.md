# README #

Quick Test task to be implemented as part of the hiring process in WMS.

### Description ###

You're a new developer on the project "Fancy File Management System" (**FFMS**). The architect has already developed some basic concepts for the new system and your first task is to write the implementation.

**FFMS** stores files in a database. Each file in the FMS can have several permissions attached to it.
Permission object defines what user can do with a file (read / write / delete). In terms of optimisation it was decided that User objects do not have any references to File objects.


### Task ###

* Implement saving / deletion of objects to any in-memory database using hibernate annotations (all private fields of File, Permission, User should be persisted to database)
* Implement IFileServiceDao interface (methods marked as //TODO)
* Implement any other DAO interfaces you might need to accomplish the task (UserDao)
* Implement tests for IFileServiceDao (any tests you find useful to prove that the service you've developed is working)


Tests should be executed with:

**mvn clean test**



### Notes ###
As part of this task you're free to use any 3rd party libraries.

You have to use hibernate and hibernate annotations.

You get extra points for using spring framework




** Good luck and happy coding.**